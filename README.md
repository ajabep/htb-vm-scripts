HTB attack VM
=============

This are scripts to create an attack VM. It's more a template than another
thing. Here is the skeleton applied for Hack The Box.

For first start, run `bash ./start.sh firstStartVM`

More command line with `bash ./start.sh -h`

--------

"We strongly recommend not to use your production PC to connect to the HTB
Network. Build a VM or physical system just for this purpose."

So, I need to protect my machine and my own network.


## What's inside

First, an attacker must not gain any access to my VM.

So:

1. Remove default access
2. Update every softwares
3. Reduce exposition surface
  1. Minimalist distro is recommended
4. Automatize the creation of the VM (to limit lifetime of VM)

Then, an attacker on my VM must not escape the VM by hypervisor or by network.

So:

1. Sandbox hypervisor (not here)
2. Update hypervisor (not here)
3. Remove of, if not possible, update extentions pack of hypervisor (not here)
4. Reduce exposition surface
  1. Disable all hypervisor options not used
  2. Remove each tool unused making interface with hypervisor (= guest tools)
  3. Control every enable options (eg: shared files)
5. Connect to a private network
6. Connect this private network only to what minimalists targets (eg: 8.8.8.8)
7. Connect this private network to internet ONLY when needed


## TODO
* Remove guest tools
