#!/bin/bash
# Start and provides VM with vagrant
# And manage internet access
# WARNING: WHEN INTERNET ACCESS IS ENABLE, IT MAY ACCESS TO INTERNAL RESSOURCES

set -ex

reserved_list=( \
	0.0.0.0/8          \
	10.0.0.0/8         \
	100.64.0.0/10      \
	127.0.0.0/8        \
	169.254.0.0/16     \
	172.16.0.0/12      \
	192.0.0.0/24       \
	192.0.2.0/24       \
	192.88.99.0/24     \
	192.168.0.0/16     \
	198.18.0.0/15      \
	198.51.100.0/24    \
	203.0.113.0/24     \
	224.0.0.0/4        \
	240.0.0.0/4        \
	255.255.255.255/32 \
)
ip_vm='192.168.50.4'

get_internet_if() {
	ip r | grep default | grep -Eo 'dev [^ ]*' | sed 's/dev //g'
}

get_vmnet_if() {
	vboxmanage showvminfo htb --machinereadable | grep hostonlyadapter2= | sed 's/.*"\([^"]*\)"/\1/g'
}

setup_firewall () {
	if_internet="$(get_internet_if)"
	if_host=$1

	# Drop by default
	sudo iptables -t filter -P FORWARD DROP

	# Add masquerading
	sudo iptables -t nat -A POSTROUTING -o $if_internet -j MASQUERADE

	# Allow direct communication with VM
	sudo iptables -t filter -A FORWARD -i $if_internet -o $if_host -d $ip_vm -j ACCEPT
	sudo iptables -t filter -A FORWARD -i lo -o $if_host -d $ip_vm -j ACCEPT

	# Remove non internet communications
	for reserved in ${reserved_list[@]}
	do
		sudo iptables -t filter -A FORWARD -i $if_host -o $if_internet -d $reserved -j REJECT --reject-with icmp-net-unreachable
		sudo iptables -t filter -A FORWARD -i $if_internet -o $if_host -d $reserved -j REJECT --reject-with icmp-net-unreachable
	done

	# Accept minimals to comunicate with htb
	sudo iptables -t filter -A FORWARD -i $if_host -o $if_internet -d 8.8.8.8 -j ACCEPT
	sudo iptables -t filter -A FORWARD -i $if_host -o $if_internet -d 88.198.233.171 -p udp --dport 1337 -j ACCEPT

	# Enable routing
	echo 1 >/proc/sys/net/ipv4/ip_forward
}

remove_firewall() {
	if_internet="$(get_internet_if)"
	if_host=$1

	disableInternet "$1" || true  # if not enabled, will fail

	# Disable routing
	echo 0 >/proc/sys/net/ipv4/ip_forward

	# Remove minimals to comunicate with htb
	sudo iptables -t filter -D FORWARD -i $if_host -o $if_internet -d 8.8.8.8 -j ACCEPT
	sudo iptables -t filter -D FORWARD -i $if_host -o $if_internet -d 88.198.233.171 -p udp --dport 1337 -j ACCEPT

	# Stop allowing direct communication with VM
	sudo iptables -t filter -D FORWARD -i $if_internet -o $if_host -d $ip_vm -j ACCEPT
	sudo iptables -t filter -D FORWARD -i lo -o $if_host -d $ip_vm -j ACCEPT

	# Remove non internet communications prohibition
	for reserved in ${reserved_list[@]}
	do
		sudo iptables -t filter -D FORWARD -i $if_host -o $if_internet -d $reserved -j REJECT --reject-with icmp-net-unreachable
		sudo iptables -t filter -D FORWARD -i $if_internet -o $if_host -d $reserved -j REJECT --reject-with icmp-net-unreachable
	done

	# Remove masquerading
	sudo iptables -t nat -D POSTROUTING -o $if_internet -j MASQUERADE
}

reset_firewall() {
	remove_firewall $*
	setup_firewall $*
}

firstStartVM() {
	vagrant up
	#echo "khm"
	vagrant halt
	vboxmanage modifyvm htb --nic1 none
	sed 's/#config.vm.network "private_network", ip: ipVM/config.vm.network "private_network", ip: ipVM/g' -i Vagrantfile
	sed 's/#config.ssh.username/config.ssh.username/g' -i Vagrantfile
	sed 's/#config.ssh.host/config.ssh.host/g' -i Vagrantfile
	sed 's/#config.ssh.port/config.ssh.port/g' -i Vagrantfile
	sed 's/#vb.customize \["modifyvm", :id, "--nic1/vb.customize \["modifyvm", :id, "--nic1/g' -i Vagrantfile
	startVM || true  # 1st time, will fails
	startVM
}

startVM() {
	echo -e '\e[31;1mFor first usage, execute "firstStartVM" instead\e[0m'
	echo -e '\e[34;1mIf this command fails, try a second time. If the failure persists, you have a problem\e[0m'
	vagrant up --no-provision
	[ "$?" != "0" ] && return 1
	int="$(get_vmnet_if)"
	setup_firewall $int
}

destroyVM() {
	int="$(get_vmnet_if)"
	vagrant destroy -f
	remove_firewall $int
}

haltVM() {
	int="$(get_vmnet_if)"
	vagrant halt
	remove_firewall $int
}

rebootVM() {
	stopVM
	startVM
}

enableInternet() {
	if_internet="$(get_internet_if)"
	if_host=$1

	sudo iptables -t filter -A FORWARD -i $if_internet -o $if_host -j ACCEPT
	sudo iptables -t filter -A FORWARD -i $if_host -o $if_internet -j ACCEPT
}

disableInternet() {
	if_internet="$(get_internet_if)"
	if_host=$1

	sudo iptables -t filter -D FORWARD -i $if_internet -o $if_host -j ACCEPT
	sudo iptables -t filter -D FORWARD -i $if_host -o $if_internet -j ACCEPT
}

help() {
	set +x
	echo "Usage: $prog COMMAND"
	echo "	COMMANDS:"
	echo "		-h		display this help message and exit"
	echo "		--help"
	echo "		help"
	echo "		"
	echo "		firstStartVM		Create and bootstrap the non-existant VM. To do so, reset the"
	echo "		            		Vagrantfile, change value which needs to be changed (PASSWORDS!)"
	echo "		            		and run this command"
	echo "		startVM			Start an existant VM"
	echo "		destroyVM		Destroy an existant VM"
	echo "		haltVM			Halt an existant VM"
	echo "		rebootVM		Reboot an existant VM"
	echo "		"
	echo "		setup_firewall IF	Set the firewall to authorize minimalist communications"
	echo "		                   	between the VM and Internet (just enought to be connecte to"
	echo "		                   	HTB VPN"
	echo "		remove_firewall IF	Remove set of firewall rules created by 'setup_firewall' cmd"
	echo "		reset_firewall IF	Remove then set the firewall"
	echo "		"
	echo "		enableInternet IF	Enable rooting from VM network to Internet, without restriction"
	echo "		disableInternet IF	Disable rooting from VM network to Internet"
	echo "		                  	Does not remove firewall rules set by 'setup_firewall'"
	echo "		"
	echo "		get_internet_if		Return the name of interface connected to Internet"
	echo "		get_vmnet_if		Return the name of interface connected to the VM nammed 'htb'"
}
--help() {
	help
}
-h() {
	--help
}

prog=$0
cmd=$1
shift
$cmd $*
